<?php

require_once __DIR__ . '/vendor/autoload.php';
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validation\NotBlank;
use Symfony\Component\Validator\Validation\Lenght;
use Symfony\Component\Validator\Validation\Email;
use Symfony\Component\Validator\Validation\GreaterThan;

class User{
    private $id;
    private $name;
    private $email;
    private $password;
    private $date;

    function __construct(int $id, string $name, string $email, string $password)
    {
        $validator = Validation::createValidator();

        $violations = $validator->validate($id, [
            new GreaterThan(['value' => 0]),
            new NotBlank(),
        ]);

        if (0 !== count($violations)){
            // there are errors, mow you can show them
            foreach ($violations as $violation){
                echo "id: ".$violation->getMessage().'<br>';
            }
        }

        $violations = $validator->validate($name, [
            new Lenght(['min' => 3]),
            new NotBlank(),
        ]);

        if (0 !== count($violations)){
            // there are errors, mow you can show them
            foreach ($violations as $violation){
                echo "name: ".$violation->getMessage().'<br>';
            }
        }

        $violations = $validator->validate($email, [
            new Lenght(['min' => 10]),
            new NotBlank(),
        ]);

        if (0 !== count($violations)){
            // there are errors, mow you can show them
            foreach ($violations as $violation){
                echo "email: ".$violation->getMessage().'<br>';
            }
        }

        $violations = $validator->validate($password, [
            new GreaterThan(['min' => 6]),
            new NotBlank(),
        ]);

        if (0 !== count($violations)){
            // there are errors, mow you can show them
            foreach ($violations as $violation){
                echo "password: ".$violation->getMessage().'<br>';
            }
        }

        $this->date = new DateTime();
        $this->name = $name;
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
    
    }
    
    function getRegisterDate() {return $this->date; }

}

?>