<?php

include("/home/miet/apps/u123456/user.php");

class Comment{

    private $user;
    private $comment;

    function __construct(User $user, string $comment)
    {
        $this->user = $user;
        $this->comment = $comment;
    }

    public function GetUser(){
        return $this->user;
    }

    public function GetComment(){
        return $this->comment;
    }
    
}

?>