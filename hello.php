<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

require_once('vendor/autoload.php');

// create a log channel
$log = new Logger('name');
$log->pushHandler(new StreamHandler('main.log', Logger::WARNING));

// add records to the log
$log->warning('Foo');
$log->error('Bar');
